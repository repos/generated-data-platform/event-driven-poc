[![Project Status: Concept – Minimal or no implementation has been done yet, or the repository is only intended to be a
limited example, demo, or
proof-of-concept.](https://www.repostatus.org/badges/latest/concept.svg)](https://www.repostatus.org/#concept)

# event-driven-poc

This repo contains an Apache Flink app that reads from the `page-create` kafka topic, selects
`enwiki` events, and queries the MediaWiki Image APIs to generate a list of image links on the page.
The output is then written back to kafka in the `platform-wiki-page-create` topic.

## Show me the data

```
$ gmodena@stat1005:~$ kafkacat -C -b kafka-jumbo1001.eqiad.wmnet:9092  -t platform-wiki-image-links -c 2
{
  "title" : "User:Fosterksam/sandbox",
  "article_id" : 70086712,
  "wiki" : "en",
  "image_urls" : [
    "File:Essay.svg",
    "File:Kwasi Adu-Gyan.jpg"
  ]
}
{
  "title" : "Draft:Hive Blockchain",
  "article_id" : 70086714,
  "wiki" : "en",
  "image_urls" : [
    "File:AFC-Logo.svg"
  ]
}
```

# Build

Run
```
./gradlew build
```
to create a thin jar, or
```
./graldew shadowJar
```
to create a fat jar with all dependencies.


# Deploy

The `shadowJar` target can be manually copied to a stat machine with:
```bash
scp build/libs/event-driven-poc-0.1-SNAPSHOT.jar stat1008.eqiad.wmnet:~/flink-1.14.3/
```

Start a Flink cluster on YARN with
```
export HADOOP_CLASSPATH=`hadoop classpath`
./bin/yarn-session.sh --detached
```

Finally lunch the job with
```bash
./bin/flink run event-driven-poc-0.1-SNAPSHOT.jar
```

## View the output of a Flink job

On YARN stdout is directed to the container job, and won't be visible from the cli.
We can display container output by accessing its logs with
```
yarn logs -applicationId <applicationId> -containerId <containerId>
```
Where 
- `<applicationId>` is the Flink cluster id returned by `yarn-session.sh`, and visible at https://yarn.wikimedia.org.
- `<containerId>` is the container running a specific task, tha you can find in Flink's Task Manager at https://yarn.wikimedia.org/proxy/<applicationId>/#/task-manager).

# Flink on YARN Quickstart

A standalone cluster can be setup locally (on a stat machine atop YARN) with
```
wget https://www.apache.org/dyn/closer.lua/flink/flink-1.14.3/flink-1.14.3-bin-scala_2.12.tgz
tar xvzf flink-1.14.3-bin-scala_2.12.tgz
cd flink-1.14.3 
export HADOOP_CLASSPATH=`hadoop classpath`
./bin/yarn-session.sh --detached
```

For more details see the [project doc](https://ci.apache.org/projects/flink/flink-docs-release-1.13/docs/deployment/resource-providers/standalone/overview/).
The [Flink Web Interface]() will be available at yarn.wikimedia.org under
https://yarn.wikimedia.org/proxy/<applicationId>.
# Config

There's a couple of gotchas.

## JVM

We need to rewrite the `Host` HTTP header to properly route HTTP request from the
internal YARN cluster to https://api-ro.discovery.wmnet.

To do so, we need to configure the JVM http-client to allow restricted headers.

Add the following to `conf/flink-conf.yaml`:
```
env.java.opts: -Dsun.net.http.allowRestrictedHeaders=true
```

## Kerberos 
Kerberos authentication is required to access WMF Analytics resources.
The relevant config settings are found in `conf/flink-conf.yaml`:
```properties
security.kerberos.login.use-ticket-cache: true
# security.kerberos.login.keytab:
security.kerberos.login.principal: krbtgt/WIKIMEDIA@WIKIMEDIA
# The configuration below defines which JAAS login contexts
security.kerberos.login.contexts: Client,KafkaClient
```


