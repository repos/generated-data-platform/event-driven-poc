package org.wikimedia.platform.events

import io.circe._
import io.circe.generic.semiauto.deriveCodec
import io.circe.syntax.EncoderOps

// TODO(gmodena): the numerical id ("6678") in the nested arraylist
//  is tricky to map to a case class without to much mangling.
case class WikiImageLinks(title: Json,
                          article_id: Json,
                          wiki: String,
                          image_urls: List[Json])

object WikiImageLinks {
  implicit val codec = deriveCodec[WikiImageLinks]

  // TODO(gmodena): to be used only for PoC purposes. This is broken. We should at least add some error handling.
  //  if a malformed record is passed, and e.g. head() is called on an empty list, an Exception will propagate and
  //  shut down the Job.
  def from(body: Json): String = {
    val imageUrls = (body findAllByKey "images") match {
      case Nil => List()
      case List() => List()
      case images: List[Json] => images.head findAllByKey "title"
    }
    encode(new WikiImageLinks(
      title = (body findAllByKey "title").head,
      article_id = (body findAllByKey "pageid").head,
      wiki = "en",
      image_urls = imageUrls
    ))
  }

  private def encode(message: WikiImageLinks): String = {
    message.asJson.toString
  }

  private def decode(jsonString: String): Option[WikiImageLinks] = {
    parser.decode[WikiImageLinks](jsonString).toOption
  }
}
