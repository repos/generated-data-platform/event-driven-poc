package org.wikimedia.platform.events.wmfapi

import org.apache.flink.streaming.api.scala.async.{AsyncFunction, ResultFuture}
import org.apache.flink.util.concurrent.Executors
import scalaj.http.{Http, HttpRequest}

import scala.concurrent.{ExecutionContext, Future}

trait Query {
  val Url: String = "https://api-ro.discovery.wmnet/w/api.php"
  val Host: String = "en.wikipedia.org"
  val Params: List[(String, String)] = List(("action", "query"), ("format", "json"))
  val Request: HttpRequest = Http(Url).headers(Seq(("Host", Host)))
}

object QueryImages extends Query {
  def apply(input: String): String =
    (Params ++ List(("prop", "images"), ("titles", input))).foldLeft[HttpRequest](Request) {
      (request, params) => request.param(params._1, params._2)
    }.asString.body
}

class AsyncQuery extends AsyncFunction[String, String] {
  implicit lazy val executor: ExecutionContext = ExecutionContext.fromExecutor(Executors.directExecutor())

  override def asyncInvoke(input: String, resultFuture: ResultFuture[String]): Unit = {
    Future {
      resultFuture.complete(Iterable(QueryImages(input)))
    }(ExecutionContext.global)
  }
}
