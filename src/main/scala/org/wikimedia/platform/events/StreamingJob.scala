package org.wikimedia.platform.events

import io.circe.Json
import io.circe.parser._
import org.apache.flink.api.common.serialization.{SimpleStringEncoder, SimpleStringSchema}
import org.apache.flink.core.fs.Path
import org.apache.flink.streaming.api.functions.sink.filesystem.StreamingFileSink
import org.apache.flink.streaming.api.functions.sink.filesystem.rollingpolicies.DefaultRollingPolicy
import org.apache.flink.streaming.connectors.kafka.{FlinkKafkaConsumer, FlinkKafkaProducer}
import org.slf4j.LoggerFactory
import org.wikimedia.platform.events.wmfapi.AsyncQuery

import java.util.Properties
import java.util.concurrent.TimeUnit

object KafkaConfig {
  val properties = new Properties();
  private val KafkaSourceTopic = "eqiad.mediawiki.page-create"
  private val KafkaSinkTopic = "platform-wiki-image-links"
  private val KafkaBootstrapServers = "kafka-jumbo1001.eqiad.wmnet:9092"
  private val KafkaGroupId = "platform-event-driven-poc"
  properties.setProperty("bootstrap.servers", KafkaBootstrapServers);
  properties.setProperty("group.id", KafkaGroupId)

  def source: FlinkKafkaConsumer[String] = new FlinkKafkaConsumer[String](KafkaSourceTopic, new SimpleStringSchema(), properties)

  def sink: FlinkKafkaProducer[String] = new FlinkKafkaProducer[String](KafkaBootstrapServers, KafkaSinkTopic, new SimpleStringSchema())
}

object StreamingFileConfig {
  private val HdfsSinkOutputPath = "hdfs:///user/gmodena/wiki-image-links/"

  def hdfsSink: StreamingFileSink[String] = StreamingFileSink
    .forRowFormat(new Path(StreamingFileConfig.HdfsSinkOutputPath), new SimpleStringEncoder[String]("UTF-8"))
    .withRollingPolicy(
      DefaultRollingPolicy.builder()
        .withRolloverInterval(TimeUnit.MINUTES.toMillis(15))
        .withInactivityInterval(TimeUnit.MINUTES.toMillis(5))
        .withMaxPartSize(1024 * 1024 * 1024)
        .build())
    .build()
}


class StreamingJob

object StreamingJob {
  private val Log = LoggerFactory.getLogger(classOf[StreamingJob])
  private val JobName = "Wiki Image Links"
  private val QueryTimeoutMs = 1000
  private val AsyncDataStreamCapacity = 12

  def main(args: Array[String]): Unit = {
    import io.circe.generic.extras.Configuration
    import io.circe.generic.extras.auto._
    import org.apache.flink.streaming.api.scala._
    implicit val config: Configuration = Configuration.default.withDefaults


    val env = StreamExecutionEnvironment.getExecutionEnvironment
    val pageStream: DataStream[String] = env.addSource(KafkaConfig.source)

    val pageTitleStream: DataStream[String] = pageStream.flatMap({ message: String =>
      decode[PageCreate](message) match {
        case Left(error) => {
          Log.error(error.toString)
          None
        }
        case Right(pageCreate: PageCreate) => Option(pageCreate)
      }
    }).filter((pageCreate: PageCreate) => pageCreate.database == "enwiki")
      .map((pageCreate: PageCreate) => (pageCreate.page_title))

    val asyncStream: DataStream[String] = AsyncDataStream.unorderedWait(pageTitleStream,
      new AsyncQuery(),
      timeout = QueryTimeoutMs,
      TimeUnit.MILLISECONDS,
      AsyncDataStreamCapacity)

    val wikiImageLinks: DataStream[String] = asyncStream.flatMap({
      message: String =>
        Log.info(message)
        parse(message) match {
          case Left(error) => {
            Log.error(error.toString)
            None
          }
          case Right(payload) => Option(payload)
        }
    }).map((payload: Json) => WikiImageLinks.from(payload))

    wikiImageLinks.addSink(KafkaConfig.sink)
    wikiImageLinks.print
    env.execute(JobName)
  }
}